<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    //

    protected $fillable = ['name', 'link'];

    public function faculties()
    {
        return $this->hasMany('App\Faculty');
    }
}
