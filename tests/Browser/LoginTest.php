<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLoginSuccessful()
    {
        $this->browse(function (Browser $browser) {
            User::create ([ 'name' => 'Testing',
                            'lastname' => 'Test',
                            'email' => 'test@test.com',
                            'password' => bcrypt('12345678')]);

            $browser->visit('/login')
                    ->type('email', 'test@test.com')
                    ->type('password','12345678')
                    ->press('Login')
                    ->waitForText('Dashboard')
                    ->assertSee('Dashboard')
                    ->logout();
        });
    }

    public function test3Failed()
    {
        $this->browse(function (Browser $browser) {
            User::create ([ 'name' => 'Testing',
                            'lastname' => 'Test',
                            'email' => 'test@test.com',
                            'password' => bcrypt('87654321')]);

            $browser->visit('/login')
                    ->type('email', 'test@test.com')
                    ->type('password','not87654321')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match')
                    ->visit('/login')
                    ->type('email', 'test@test.com')
                    ->type('password','not87654321')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match')
                    ->visit('/login')
                    ->type('email', 'test@test.com')
                    ->type('password','not87654321')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match')
                    ->logout();
        });
    }
}
