<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PasswordTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            User::create ([ 'name' => 'Testing',
                            'lastname' => 'Testing',
                            'email' => 'test@test.com',
                            'password' => bcrypt('12345678')]);

            $browser->visit('/login')
                    ->type('email', 'test@test.com')
                    ->type('password','123456789')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match');
        });
    }
}
